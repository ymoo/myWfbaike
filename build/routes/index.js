"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var http = require("http");
var server_1 = require("../config/server");
var router = express.Router();
/* GET home page. */
router.use('/*', function (req, res, next) {
    var option = getServerOption(req);
    console.info(option);
    if (option) {
        var creq = http.request(option, function (cres) {
            res.writeHead(200, { 'Content-Type': 'text/html;charset=utf-8' }); //设置response编码为utf-8
            cres.pipe(res);
        });
        req.pipe(creq);
    }
    else {
        next();
    }
    // res.render('index', { title: 'Express' });
});
var getServerOption = function (req) {
    var urlArr = req.originalUrl.split('/');
    var pathName = urlArr.shift();
    pathName = pathName == '' ? urlArr.shift() : pathName;
    if (server_1.default.servers[pathName]) {
        return {
            host: server_1.default.servers[pathName][0].host,
            port: server_1.default.servers[pathName][0].port,
            path: urlArr.join('/')
        };
    }
    else {
        return null;
    }
};
exports.default = router;
