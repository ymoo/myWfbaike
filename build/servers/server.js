/**
 * Created by ASUS on 2/28 0028.
 */
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
/**
 * 创建服务
 * @returns {any}
 */
var createServer = function (routeName) {
    var router = require("./" + routeName);
    var route = express();
    route.set('views', path.join(__dirname, '../views'));
    route.set('view engine', 'jade');
    // uncomment after placing your favicon in /public
    //route.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
    route.use(logger('dev'));
    route.use(bodyParser.json());
    route.use(bodyParser.urlencoded({ extended: false }));
    route.use(cookieParser());
    route.use(express.static(path.join(__dirname, '../public')));
    route.use('/', router);
    // catch 404 and forward to error handler
    route.use(function (req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });
    // error handler
    route.use(function (err, req, res, next) {
        // set locals, only providing error in development
        res.locals.message = err.message;
        res.locals.error = req.app.get('env') === 'development' ? err : {};
        // render the error page
        res.status(err.status || 500);
        res.render('error');
    });
    return route;
};
module.exports = createServer;
