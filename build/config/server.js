/**
 * 应用配置文件
 * @type {{}}
 */
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var config = {
    c1: {
        proxyServers: [
            {
                id: '1',
                host: '192.168.0.200',
                port: 3002,
                cup: 0,
                debug: true,
                args: '--inspect=7000',
            },
        ],
        servers: {
            admin: [
                { id: '1', host: '192.168.0.200', port: 3010, args: '--inspect=7001', debug: true },
                { id: '2', host: '192.168.0.200', port: 3011, args: '--inspect=7002', debug: true },
            ],
        }
    },
    c2: {
        proxyServers: [
            {
                id: '1',
                host: '127.0.0.1',
                port: 3002,
                cup: 0,
                debug: true,
                args: '--inspect=7000',
            },
        ],
        servers: {
            admin: [
                { id: '1', host: '127.0.0.1', port: 3010, args: '--inspect=7001', debug: true },
                { id: '2', host: '127.0.0.1', port: 3011, args: '--inspect=7002', debug: true },
            ],
        }
    }
};
var c = config.c2;
exports.default = c;
