"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by ASUS on 2/28 0028.
 *
 * 服务状态     status [
 * -1：初始化，
 * 0：服务创建成功，
 * 1：（服务创建错误）
 * 2：requires elevated privileges 权限不足（服务创建错误）
 * 3：端口is already in use 端口被占用（服务创建错误）
 * 4：进程关闭（服务创建错误）
 * ]
 */
var config = require("../config");
var child_process = require("child_process");
var path = require("path");
var os = require("os");
var manager_1 = require("./manager");
var master = (function () {
    function master() {
        this.ProcessPool = {}; //进程池
        this.configs = {
            master: {},
            proxyServers: [],
            servers: {} //微服、模块服务
        };
        this.serversPool = {};
        this.localIp = '0.0.0.0'; //本机ip
        this.setConfig();
        this.getLocalIp();
        this.createSocket();
        this.createStartFun();
    }
    /**
     * 设置配置信息
     */
    master.prototype.setConfig = function () {
        this.configs.master = config.master;
        this.configs.servers = config.server.servers;
        this.configs.proxyServers = config.server.proxyServers;
    };
    /**
     * 获取本机ip
     */
    master.prototype.getLocalIp = function () {
        var interfaces = os.networkInterfaces();
        for (var devName in interfaces) {
            var iface = interfaces[devName];
            for (var i = 0; i < iface.length; i++) {
                var alias = iface[i];
                if (alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal) {
                    this.localIp = alias.address;
                    return alias.address;
                }
            }
        }
    };
    /**
     * 创建socket服务管理服务器
     */
    master.prototype.createSocket = function () {
        var _this = this;
        this.testIsLocal(this.configs.master, function () {
            _this.manager = new manager_1.default(_this.configs.master);
            _this.onManagerMsg();
        });
    };
    /**
     * 监听服务消息
     */
    master.prototype.onManagerMsg = function () {
        var _this = this;
        this.manager.http$Error = function (data) { _this.http$Error(data); };
        this.manager.http$ServerCreateSuccess = function (data) { _this.http$ServerCreateSuccess(data); };
    };
    master.prototype.http$Error = function (data) {
        console.info(data);
    };
    master.prototype.http$ServerCreateSuccess = function (data) {
        console.info(data);
        this.serversPool[data.serverType + "s"][data.serverName][data.serverId].status = data.code;
        this.serversPool[data.serverType + "s"][data.serverName][data.serverId].msg = data.msg;
        console.info(this);
    };
    /**
     * 创建服务启动方法
     */
    master.prototype.createStartFun = function () {
        var _this = this;
        // this.startServer(`serverName=app`, config.master, 'app')
        this.configs.proxyServers.map(function (vv) {
            _this.startServer('app', vv, 'proxyServer');
        });
        //创建模块服务
        Object.keys(this.configs.servers).map(function (v) {
            var server = _this.configs.servers[v];
            server.map(function (vv) {
                _this.startServer(v, vv, 'server');
            });
        });
    };
    /**
     * 开始服务进程
     * @param route
     * @param serverConfig
     * @param serverType
     */
    master.prototype.startServer = function (route, serverConfig, serverType) {
        var _this = this;
        this.testIsLocal(serverConfig, function () {
            var serverName = route;
            var host = serverConfig.host;
            var port = serverConfig.port;
            var debug = serverConfig.debug;
            var cpu = serverConfig.cpu || 0;
            var serverId = serverType + "_" + route + "_" + serverConfig.id;
            var status = -1;
            var msg = '初始化成功';
            _this.serversPool[serverType + "s"] = _this.serversPool[serverType + "s"] || {};
            _this.serversPool[serverType + "s"][route] = _this.serversPool[serverType + "s"][route] || {};
            _this.serversPool[serverType + "s"][route][serverId] = { serverType: serverType, serverName: serverName, host: host, port: port, debug: debug, cpu: cpu, serverId: serverId, status: status, msg: msg };
            var serverProcess = child_process.exec("node " + serverConfig.args + "  " + path.join(__dirname, 'www.js ') + " serverType=" + serverType + " serverName=" + serverName + " host=" + host + "  port=" + port + " debug=" + debug + " cpu=" + cpu + " serverId=" + serverId, {
                encoding: 'utf8',
                timeout: 0,
                maxBuffer: 5000 * 1024 * 2,
                killSignal: 'SIGTERM'
            }, function (error, stdout, stderr) {
                if (error) {
                    console.log(error.stack);
                    console.log('Error code: ' + error.code);
                    console.log('Signal received: ' + error.signal);
                }
                console.log('stdout: ' + stdout);
                console.log('stderr: ' + stderr);
            });
            serverProcess.on('exit', function (code) {
                console.log('子进程已退出，退出码 ' + code);
            });
            _this.ProcessPool[route] = _this.ProcessPool[route] || [];
            _this.ProcessPool[route].push(serverProcess);
        });
    };
    /**
     * 检查是否为本机
     */
    master.prototype.testIsLocal = function (serverConfig, success, fail) {
        if (success === void 0) { success = function () { }; }
        if (fail === void 0) { fail = function () { }; }
        if (this.localIp != serverConfig.host
            && serverConfig.host !== '127.0.0.1') {
            fail();
        }
        else {
            success();
        }
    };
    return master;
}());
var serverMaster = new master();
