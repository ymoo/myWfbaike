#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var cluster = require("cluster");
var os = require("os");
var ioClient = require("socket.io-client");
var logger = require("morgan");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var master_1 = require("../config/master");
var debug = require('debug')('wfbaike:server');
var http = require('http');
var app;
var serverType = process.argv[2].split('=')[1];
var serverName = process.argv[3].split('=')[1];
var host = process.argv[4].split('=')[1];
var port = process.argv[5].split('=')[1];
var _debug = process.argv[6].split('=')[1];
var numCPUs = process.argv[7].split('=')[1] || os.cpus().length;
var serverId = process.argv[8].split('=')[1];
/**
 * 创建服务
 */
var createServer = function () {
    var t = {
        'proxyServer': function () {
            app = require("./app");
        },
        'server': function () {
            var createServer = require("../server");
            app = createServer(serverName);
        }
    };
    console.info('%$$$$$$$$$$$$$$$$$$$$$$$');
    console.info(serverType);
    t[serverType]();
};
createServer();
var run = function (cluster) {
    var masterClient = ioClient('http://' + master_1.default.host + ':' + master_1.default.port);
    console.info(masterClient);
    app.set('masterClient', masterClient);
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(cookieParser());
    app.use(logger('dev'));
    // catch 404 and forward to error handler
    app.use(function (req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });
    // error handler
    app.use(function (err, req, res, next) {
        // set locals, only providing error in development
        res.locals.message = err.message;
        res.locals.error = req.app.get('env') === 'development' ? err : {};
        // render the error page
        res.status(err.status || 500);
        res.render('error');
    });
    port = normalizePort(process.env.PORT || port);
    app.set('port', port);
    app.set('cluster', cluster);
    app.set('serverId', serverId);
    /**
     * Normalize a port into a number, string, or false.
     */
    function normalizePort(val) {
        var port = parseInt(val, 10);
        if (isNaN(port)) {
            // named pipe
            return val;
        }
        if (port >= 0) {
            // port number
            return port;
        }
        return false;
    }
    /**
     * 获取消息
     */
    var getMsg = function (msg, code) {
        return {
            serverType: serverType,
            serverId: serverId,
            serverName: serverName,
            code: code,
            msg: msg
        };
    };
    /**
     * 创建服务
     * @param socket
     */
    var createServer = function (socket) {
        /**
         * Create HTTP server.
         */
        console.info(socket);
        //发送http服务创建成功通知到主机
        masterClient.emit('onHttp$ServerCreateSuccess', getMsg("\u670D\u52A1\u521B\u5EFA\u6210\u529F-serverType=" + serverType + "|serverName=" + serverName, 0));
    };
    /**
     * Event listener for HTTP server "error" event.
     */
    function onError(error) {
        var msg = '';
        var code = 0;
        if (error.syscall !== 'listen') {
            code = 1;
            msg = JSON.stringify(error);
            throw error;
        }
        var bind = typeof port === 'string'
            ? 'Pipe ' + port
            : 'Port ' + port;
        // handle specific listen errors with friendly messages
        switch (error.code) {
            case 'EACCES':
                console.error(bind + ' requires elevated privileges');
                process.exit(1);
                msg = bind + ' requires elevated privileges';
                code = 2;
                break;
            case 'EADDRINUSE':
                console.error(bind + ' is already in use');
                process.exit(1);
                msg = bind + ' is already in use';
                code = 3;
                break;
            default:
                msg = JSON.stringify(error);
                code = 4;
                throw error;
        }
        /**
         * 错误通知
         */
        masterClient.emit('onHttp$Error', getMsg(msg, code));
    }
    /**
     * Event listener for HTTP server "listening" event.
     */
    function onListening() {
        var addr = server.address();
        var bind = typeof addr === 'string'
            ? 'pipe ' + addr
            : 'port ' + addr.port;
        debug('Listening on ' + bind);
    }
    var server = http.createServer(app);
    /**
     * 监听http服务端口
     */
    server.listen(port, host);
    server.on('error', onError);
    server.on('listening', onListening);
    /**
     * 监听master主机连接成功
     */
    masterClient.on('connect', createServer);
};
if (cluster.isMaster && !_debug) {
    for (var i = 0; i < numCPUs; i++) {
        cluster.fork();
        console.info('进程id：' + i);
    }
    cluster.on("exit", function (worker, code, signal) {
        console.info(worker);
        console.info(code);
        console.info(signal);
        cluster.fork();
    });
}
else {
    run(cluster);
}
