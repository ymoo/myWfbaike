"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var io = require("socket.io");
var http = require('http');
var manager = (function () {
    function manager(config) {
        this.config = {
            host: '127.0.0.1',
            port: '3003'
        };
        this.onClientMessage = function (data) { };
        this.onDisconnect = function () { };
        this.http$Error = function (data) { };
        this.http$ServerCreateSuccess = function (data) { };
        this.config = config;
        this.init();
    }
    manager.prototype.init = function () {
        var _this = this;
        //通过listen或attach绑定
        this.httpServer = http.createServer();
        this.ioSocket = io(this.httpServer);
        // io.attach(httpServer);
        this.httpServer.listen(this.config.port, this.config.host);
        console.info("ws\u670D\u52A1\u542F\u52A8\u6210\u529F~");
        console.info("ws://" + this.config.host + ":" + this.config.port);
        // 服务端监听连接状态：io的connection事件表示客户端与服务端成功建立连接，它接收一个回调函数，回调函数会接收一个socket参数。
        this.ioSocket.on('connection', function (socket) {
            _this.connection(socket);
        });
    };
    manager.prototype.connection = function (socket) {
        this.socket = socket;
        console.log('client connect server, ok!');
        // io.emit()方法用于向服务端发送消息，参数1表示自定义的数据名，参数2表示需要配合事件传入的参数
        socket.emit('serverMessage', { msg: 'client connect server success' });
        // socket.broadcast.emmit()表示向除了自己以外的客户端发送消息
        // 监听断开连接状态：socket的disconnect事件表示客户端与服务端断开连接
        socket.on('disconnect', this.onDisconnect);
        // 与客户端对应的接收指定的消息
        socket.on('clientMessage', this.onClientMessage);
        //监听http服务出错消息
        socket.on('onHttp$Error', this.http$Error);
        //监听http服务创建成功消息
        socket.on('onHttp$ServerCreateSuccess', this.http$ServerCreateSuccess);
        // socket.disconnect();
    };
    /**
     * 发送消息到客户端
     * socket.broadcast.emmit()表示向除了自己以外的客户端发送消息
     */
    manager.prototype.sendMessage = function (data) {
        if (this.socket) {
            this.socket.broadcast.emmit('serverMessage', data);
        }
        else {
            console.error('socket 未连接');
        }
    };
    return manager;
}());
exports.default = manager;
