"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var path = require('path');
var favicon = require('serve-favicon');
var router_1 = require("./router");
var app = express();
// view engine setup
app.set('views', path.join(__dirname, '../views'));
app.set('view engine', 'jade');
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(express.static(path.join(__dirname, '../public')));
console.info(path.join(__dirname, '../public'));
app.use('/', router_1.default);
module.exports = app;
