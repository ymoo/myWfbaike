/**
 * Created by ASUS on 2/28 0028.
 */
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
/**
 * 创建服务
 * @returns {any}
 */
var createServer = function (routeName) {
    var router = require("./servers/" + routeName);
    var route = express();
    route.set('views', path.join(__dirname, './views'));
    route.set('view engine', 'jade');
    // uncomment after placing your favicon in /public
    //route.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
    route.use('/', router);
    route.use(express.static(path.join(__dirname, './public')));
    return route;
};
module.exports = createServer;
