/**
 * 应用配置文件
 * @type {{}}
 */

const config = {
    c1:{
        proxyServers:[          //代理服务器
            {
                id:'1',
                host: '192.168.0.200',
                port: 3002,
                cup: 0,                  //开启进程数量 0为自动（根据机器cpu数量开启相应进程个数）
                debug: true,             //开启调试将不开启cluster
                args: '--inspect=7000',
            },
        ],
        servers:{
            admin:[
                {id:'1',host:'192.168.0.200',port:3010,args:'--inspect=7001',debug:true},
                {id:'2',host:'192.168.0.200',port:3011,args:'--inspect=7002',debug:true},
            ],
            // user:[
            //     {id:'1',host:'192.168.0.200',port:3020,args:'--inspect=7003'},
            //     {id:'2',host:'192.168.0.200',port:3021,args:'--inspect=7004'},
            // ]
        }
    },
    c2:{
        proxyServers:[          //代理服务器
            {
                id:'1',
                host: '127.0.0.1',
                port: 3002,
                cup: 0,                  //开启进程数量 0为自动（根据机器cpu数量开启相应进程个数）
                debug: true,             //开启调试将不开启cluster
                args: '--inspect=7000',
            },
        ],
        servers:{
            admin:[
                {id:'1',host:'127.0.0.1',port:3010,args:'--inspect=7001',debug:true},
                {id:'2',host:'127.0.0.1',port:3011,args:'--inspect=7002',debug:true},
            ],
            // user:[
            //     {id:'1',host:'192.168.0.200',port:3020,args:'--inspect=7003'},
            //     {id:'2',host:'192.168.0.200',port:3021,args:'--inspect=7004'},
            // ]
        }
    }
}

var c = config.c2
export default c