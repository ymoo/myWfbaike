import * as express from 'express';
import * as http from 'http'
import config from '../config/server'

const router = express.Router();

/* GET home page. */
router.use('/*', function (req, res, next) {
    var option = getServerOption(req)
    console.info(option)
    if (option) {
        var creq = http.request(option, (cres) => {
            res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'});//设置response编码为utf-8
            cres.pipe(res)
        })
        req.pipe(creq)
    } else {
        next()
    }
    // res.render('index', { title: 'Express' });
});

const getServerOption = (req) => {
    var urlArr = req.originalUrl.split('/')
    var pathName = urlArr.shift()
    pathName = pathName == '' ? urlArr.shift() : pathName
    if (config.servers[pathName]) {
        return {
            host: config.servers[pathName][0].host,
            port: config.servers[pathName][0].port,
            path: urlArr.join('/')
        }
    } else {
        return null
    }

}

export default router;
