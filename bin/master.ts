/**
 * Created by ASUS on 2/28 0028.
 *
 * 服务状态     status [
 * -1：初始化，
 * 0：服务创建成功，
 * 1：（服务创建错误）
 * 2：requires elevated privileges 权限不足（服务创建错误）
 * 3：端口is already in use 端口被占用（服务创建错误）
 * 4：进程关闭（服务创建错误）
 * ]
 */
import * as config from '../config'
import * as child_process from 'child_process'
import * as path from 'path'
import * as os from 'os'
import manager from './manager'

class master {
    public ProcessPool = {}             //进程池
    public configs = {                  //服务配置信息
        master: {},                     //连接分发、管理服务
        proxyServers: [],
        servers: {}                     //微服、模块服务
    }
    public serversPool={}
    public manager:any
    public localIp = '0.0.0.0'          //本机ip
    constructor() {
        this.setConfig()
        this.getLocalIp()
        this.createSocket()
        this.createStartFun()
    }

    /**
     * 设置配置信息
     */
    setConfig() {
        this.configs.master = config.master
        this.configs.servers = config.server.servers
        this.configs.proxyServers = config.server.proxyServers
    }

    /**
     * 获取本机ip
     */
    getLocalIp() {
        var interfaces = os.networkInterfaces();
        for(var devName in interfaces){
            var iface = interfaces[devName];
            for(var i=0;i<iface.length;i++){
                var alias = iface[i];
                if(alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal){
                    this.localIp = alias.address
                    return alias.address;
                }
            }
        }
    }

    /**
     * 创建socket服务管理服务器
     */
    createSocket(){
        this.testIsLocal(
            this.configs.master,
            ()=>{
                this.manager = new manager(this.configs.master)
                this.onManagerMsg()
            })
    }

    /**
     * 监听服务消息
     */
    onManagerMsg(){
        this.manager.http$Error = (data)=>{this.http$Error(data)}
        this.manager.http$ServerCreateSuccess = (data)=>{this.http$ServerCreateSuccess(data)}
    }

    http$Error(data){
        console.info(data)
    }

    http$ServerCreateSuccess(data){
        console.info(data)
        this.serversPool[`${data.serverType}s`][data.serverName][data.serverId].status = data.code
        this.serversPool[`${data.serverType}s`][data.serverName][data.serverId].msg = data.msg
        console.info(this)
    }


    /**
     * 创建服务启动方法
     */
    createStartFun() {
        // this.startServer(`serverName=app`, config.master, 'app')
        this.configs.proxyServers.map((vv) => {
            this.startServer('app', vv, 'proxyServer')
        })

        //创建模块服务
        Object.keys(this.configs.servers).map((v) => {
            var server = this.configs.servers[v]
            server.map((vv) => {
                this.startServer(v, vv, 'server')
            })
        })
    }

    /**
     * 开始服务进程
     * @param route
     * @param serverConfig
     * @param serverType
     */
    startServer(route, serverConfig, serverType) {
        this.testIsLocal(serverConfig,()=>{
            var serverName = route
            var host = serverConfig.host
            var port = serverConfig.port
            var debug = serverConfig.debug
            var cpu = serverConfig.cpu || 0
            var serverId = `${serverType}_${route}_${serverConfig.id}`
            var status = -1
            var msg = '初始化成功'
            this.serversPool[`${serverType}s`] = this.serversPool[`${serverType}s`]||{}
            this.serversPool[`${serverType}s`][route] = this.serversPool[`${serverType}s`][route]||{}
            this.serversPool[`${serverType}s`][route][serverId] = {serverType,serverName,host,port,debug,cpu,serverId,status,msg}
            var serverProcess =
                child_process.exec(
                    `node ${serverConfig.args}  ${path.join(__dirname, 'www.js ')} serverType=${serverType} serverName=${serverName} host=${host}  port=${port} debug=${debug} cpu=${cpu} serverId=${serverId}`,
                    {
                        encoding: 'utf8',
                        timeout: 0,
                        maxBuffer: 5000 * 1024 * 2, // 默认 200 * 1024
                        killSignal: 'SIGTERM'
                    },
                    function (error, stdout, stderr) {
                        if (error) {
                            console.log(error.stack);
                            console.log('Error code: ' + error.code);
                            console.log('Signal received: ' + error.signal);
                        }
                        console.log('stdout: ' + stdout);
                        console.log('stderr: ' + stderr);
                    });

            serverProcess.on('exit', function (code) {
                console.log('子进程已退出，退出码 ' + code);
            });
            this.ProcessPool[route] = this.ProcessPool[route] || []
            this.ProcessPool[route].push(serverProcess)
        })
    }

    /**
     * 检查是否为本机
     */
    testIsLocal(serverConfig,success=()=>{},fail=()=>{}){
        if (
            this.localIp != serverConfig.host
            && serverConfig.host!=='127.0.0.1'
        ) {
            fail()
        }else{
            success()
        }
    }
}

var serverMaster = new master()










